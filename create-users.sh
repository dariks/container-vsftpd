#!/bin/bash
# Script to add users to container at startup of container
USERSFILE="/etc/vsftpd.lnxusers"

if [ ! -f $USERSFILE ]
then
    echo "$USERSFILE doesn't exist. sleeping for 5 seconds and exiting."
    echo "Format for file is: username:password:/path/to/home:backups for system xy"
    exit 1
else
    echo "creating users.. please wait."
    echo "clearing vsftpd.userlist file."
    echo "" > /etc/vsftpd.userlist
fi

while read -r line;
do
    #echo "$line"
    USER="$(echo $line | cut -d':' -f1)"
    PASSWORD="$(echo $line | cut -d':' -f2)"
    USERPATH="$(echo $line | cut -d':' -f3)"
    DESCRIPTION="$(echo $line | cut -d':' -f4)"
    echo "adding user $USER"
    useradd -s /bin/bash -M -c "$DESCRIPTION" -d "$USERPATH" -G ftp $USER
    echo "$USER:$PASSWORD" | chpasswd
    echo "$USER" >> /etc/vsftpd.userlist
    chown -R $USER:$USER $USERPATH
done < $USERSFILE

echo "user script done, sleeping 3 seconds..."
sleep 3
