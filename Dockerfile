FROM debian:stable

RUN rm -f /etc/motd && touch /etc/motd && \
    apt-get update && apt-get upgrade -y && \
    mkdir -p /etc/openssh /var/run/openssh && \
    chmod 700 /etc/openssh /var/run/openssh && \
    apt-get install htop bash iproute2 bash-completion vsftpd openssh-server -y

COPY create-users.sh /usr/local/bin/

ENTRYPOINT /usr/local/bin/create-users.sh && service vsftpd start && service ssh start && /bin/sleep infinity